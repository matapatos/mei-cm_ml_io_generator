classdef GlobalMeta
    %GLOBALMETA Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
       nr_frames,
       nr_frames_with_note, 
       nr_frames_without_note, 
       nr_notes_per_frame, 
       nr_of_notes, 
       nr_of_notes_by_noteid,
       nr_of_notes_played_by_music,
       
       nr_of_sounds,
       nr_of_sounds_as_musics,
       nr_of_sounds_as_chrods_random,
       nr_of_sounds_as_chrods_ucho,
       nr_of_sounds_as_single_notes,
       
       nr_of_sounds_with_piano,
       nr_of_notes_per_music,
       
       nr_of_notes_played,
       nr_of_notes_played_by_noteid,
       
       first_note,
       max_number_of_notes_played_simultaneously,
       number_of_notes
       frame_samples, 
       overlap_ratio, 
       data_type,
       frequency_samples
    end
    
    methods
        function self = GlobalMeta(number_of_notes, max_number_of_notes_played_simultaneously, first_note, frequency_samples, frame_samples, data_type, overlap_ratio)
            %GLOBALMETA Construct an instance of this class
            %   Detailed explanation goes here
            self.nr_frames = 0;
            self.nr_of_notes = 0;
            self.nr_frames_with_note = 0;
            self.nr_frames_without_note = 0;
            self.nr_notes_per_frame = 0;
       
            self.nr_of_sounds = 0;
            self.nr_of_sounds_as_musics = 0;
            self.nr_of_sounds_as_chrods_random = 0;
            self.nr_of_sounds_as_chrods_ucho = 0;
            self.nr_of_sounds_as_single_notes = 0;
       
            self.nr_of_sounds_with_piano = containers.Map;
            
            self.first_note = first_note;
            self.number_of_notes = number_of_notes;
            self.max_number_of_notes_played_simultaneously = max_number_of_notes_played_simultaneously;
            self.nr_of_notes_by_noteid = zeros(1, self.number_of_notes);
            self.nr_of_notes_per_music = zeros(1, self.number_of_notes);
            self.nr_of_notes_played = zeros(self.max_number_of_notes_played_simultaneously, 1);
            self.nr_of_notes_played_by_noteid = zeros(self.max_number_of_notes_played_simultaneously, self.number_of_notes);
            self.frame_samples = frame_samples; 
            self.overlap_ratio = overlap_ratio;
            self.data_type = data_type;
            self.frequency_samples = frequency_samples;
        end
        
        function self = AddMusicMeta(self, l_piano_type, l_music_type, m_nrFrames, m_nrOfNotes, m_nrFramesWithNote, m_nrFramesWithoutNote, m_nrNotesPerFrame, m_nrOfNotesById, m_nrOfNotesPerMusic, m_nrOfNotesPlayed, m_nrOfNotesPlayedByNoteId)
            self = self.AddPianoType(l_piano_type);
            self = self.AddMusicType(l_music_type);
            
            self.nr_frames = self.nr_frames + m_nrFrames;
            self.nr_of_notes = self.nr_of_notes + m_nrOfNotes;
            self.nr_frames_with_note = self.nr_frames_with_note + m_nrFramesWithNote;
            self.nr_frames_without_note = self.nr_frames_without_note + m_nrFramesWithoutNote;
            self.nr_notes_per_frame = self.SumDifferentDimensionsArrays(self.nr_notes_per_frame, m_nrNotesPerFrame);
            self.nr_of_notes_by_noteid = self.nr_of_notes_by_noteid + m_nrOfNotesById;
            self.nr_of_notes_per_music = self.nr_of_notes_per_music + m_nrOfNotesPerMusic;
            
            self.nr_of_notes_played = self.nr_of_notes_played + m_nrOfNotesPlayed;
            self.nr_of_notes_played_by_noteid = self.nr_of_notes_played_by_noteid + m_nrOfNotesPlayedByNoteId;
        end
        
        function WriteDataToFile(self, filename, configsFilepath)
            fid = fopen(filename, 'a');
            fprintf(fid, self.GetHeadersAsChar());
            fprintf(fid, "\n" + self.GetValuesAsString());
            fclose(fid); 
            
            self.WriteConfigFile(configsFilepath);
        end
    end
    
    methods(Access = private)
        function WriteConfigFile(self, filename)            
            %Description section
            data = "'''This file is composed by three separate sections:\n\n\t-> Dataset information\t- contains few information's of the dataset itself, such as total of frames.\n\t-> Global Metrics\t- contains metrics in a geral global mode.\n\t-> Notes Metrics \t- contains 88 sub-sections in which each one corresponds to only one note.\n\n\nMetrics tables columns description:\n\t-> 'Chord size'\t\t\t- Number of notes in a frame (if a frame has only one chord of 5 notes, then the chord size is 5)\n\t-> 'Global %%'\t\t\t- Percentage of the 'Nr frames' of that corresponding line by the 'Nr frames' of the all dataset (section Dataset information)\n\t-> 'Nr frames' \t\t\t- Number of frames.\n\t-> 'Simultaneously %%'\t- Percentage of the 'Nr frames' of that corresponding line by the total of frames with the same 'Chord size'.\n\t-> 'Local %%'\t\t\t- Percentage of the 'Nr frames' of that corresponding line by the 'Nr frames' of that specific note.\n\n\tIMPORTANT: All the percentage metrics shown in this tables are rounded until the 2nd decimal case.'''\n\n";
            %Dataset information section
            data = data + "--- Dataset information ---\n";
            data = data + "Nr frames\t" + num2str(self.nr_frames) + "\n";
            data = data + "Nr frames with note(s):\t" + num2str(self.nr_frames_with_note) + "\n\n";
            %Global metrics section
            data = data + "----- Global Metrics ------\n\n";
            data = data + "Chord size\t\tGlobal %%\tNr frames\n\n";
            percentage = round((self.nr_frames_without_note/self.nr_frames)*100, 2);
            data = data + "0\t\t\t\t" + num2str(percentage, '%1.2f') + "%%\t\t" + num2str(self.nr_frames_without_note) + "\n";
            for i = 1 : self.max_number_of_notes_played_simultaneously
                data = self.GetInfoGlobalNoteSimultaneouslyMetrics(data, i);
            end
            %Notes metrics section
            data = data + "\n----- Notes Metics -------";
            for i = 1 : self.number_of_notes
                note_number = self.first_note + i - 1;
                data = data + "\nNote " + num2str(note_number) + ":\n";
                data = data + "\tNr of frames: " + num2str(self.nr_of_notes_by_noteid(i)) + "\n";
                percentage = round((self.nr_of_notes_by_noteid(i) / self.nr_frames)*100, 2);
                data = data +  "\tRatio with total of frames: " + num2str(percentage, '%1.2f') + "%%\n\n";
                data = data + "\tChord size\t\tGlobal %%\tSimultaneously %%\tLocal %%\t\tNr frames\n\n";
                for j = 1 : self.max_number_of_notes_played_simultaneously
                    data = self.GetInfoNoteSimultaneouslyMetrics(data, j, i);
                end
            end
            
            %Write to file
            fid = fopen(filename, 'a');
            fprintf(fid, data);
            fclose(fid);
            
            sprintf("\nMore configurations and metrics in '" + filename + "'");
        end
        
        function data = GetInfoGlobalNoteSimultaneouslyMetrics(self, data, noteIndex)
            nr_times_note_played = self.nr_of_notes_played(noteIndex);
            percentage = 0;
            if self.nr_frames > 0
                percentage = round((nr_times_note_played/self.nr_frames)*100, 2);                        
            end
            data = data + num2str(noteIndex) + "\t\t\t\t" + num2str(percentage, '%1.2f') + "%%\t\t" + num2str(nr_times_note_played) + "\n";
        end
        
        function data = GetInfoNoteSimultaneouslyMetrics(self, data, number_of_simultaneous_notes, note_index)
            nr_times_note_played = self.nr_of_notes_played_by_noteid(number_of_simultaneous_notes,note_index);
            global_percentage = 0;
            local_percentage = 0;
            num_of_notes_percentage = 0;
            
            if self.nr_frames > 0
                global_percentage = round((nr_times_note_played/self.nr_frames)*100,2);
            end
            if self.nr_of_notes_by_noteid(note_index) > 0
                local_percentage = round((nr_times_note_played/self.nr_of_notes_by_noteid(note_index))*100, 2);
            end
            if self.nr_of_notes_played(number_of_simultaneous_notes) > 0
                num_of_notes_percentage = round((nr_times_note_played/self.nr_of_notes_played(number_of_simultaneous_notes))*100,2);
            end
            
            data = data + "\t" + num2str(number_of_simultaneous_notes) + "\t\t\t\t" + num2str(global_percentage, '%1.2f') + "%%\t\t" + num2str(num_of_notes_percentage, '%1.2f') + "%%\t\t\t\t" + num2str(local_percentage, '%1.2f') + "%%\t\t" + num2str(nr_times_note_played) + "\n"; 
        end
        
        function new_array = SumDifferentDimensionsArrays(self, a_one, a_sec)
           o_s = size(a_one, 2);
            a_s = size(a_sec, 2);
            new_size = max(o_s, a_s);
            new_array = zeros(1, new_size);
            for i = 1 : new_size
               if i > o_s
                  new_array(i) =  a_sec(i);
               elseif i > a_s
                   new_array(i) = a_one(i);
               else
                   new_array(i) = a_one(i) + a_sec(i);
               end
            end 
        end
        
        function self = AddPianoType(self, piano_type)
            key = char(piano_type);
            if ~self.nr_of_sounds_with_piano.isKey(key)
                self.nr_of_sounds_with_piano(key) = 0;
            end
            
            self.nr_of_sounds_with_piano(key) = self.nr_of_sounds_with_piano(key) + 1;
        end
        
        function self = AddMusicType(self, music_type)
            self.nr_of_sounds = self.nr_of_sounds + 1;
            if music_type == MusicType.SINGLE_NOTE
               self.nr_of_sounds_as_single_notes = self.nr_of_sounds_as_single_notes + 1;
            elseif music_type == MusicType.CHORD_RAND
                self.nr_of_sounds_as_chrods_random = self.nr_of_sounds_as_chrods_random + 1;
            elseif music_type == MusicType.CHORD_UCHO
                self.nr_of_sounds_as_chrods_ucho = self.nr_of_sounds_as_chrods_ucho + 1;
            elseif music_type == MusicType.MUSIC
                self.nr_of_sounds_as_musics = self.nr_of_sounds_as_musics + 1;
            else 
                error = "ERROR: Unknown music type " + char(music_type)
            end
        end
        
        function headers = GetHeadersAsChar(self)
            %Headers
            STR_piano_types = strjoin(self.nr_of_sounds_with_piano.keys, '_piano_type,');
            STR_piano_types = STR_piano_types + "_piano_type"; %Add last one stuff
            headers = STR_piano_types + ",frequency_samples,frame_samples,data_type,overlap_ratio,nr_of_sounds,nr_sounds_as_musics,nr_sounds_as_chord_random,nr_sounds_as_chord_ucho,nr_sounds_as_siglenote,nr_frames,nr_of_notes,nr_frames_with_note,nr_frames_without_note,nr_notes_per_frame,nr_of_notes_by_noteid,nr_of_notes_per_music,nr_of_notes_played,nr_of_notes_played_by_noteid";
            headers = char(headers);
        end
        
        function values = GetValuesAsString(self)
            STR_nr_of_notes_by_noteid = mat2str(self.nr_of_notes_by_noteid);
            STR_nr_notes_per_frame = mat2str(self.nr_notes_per_frame);
            STR_nr_of_piano_types = strjoin(string(self.nr_of_sounds_with_piano.values), ',');
            STR_nr_of_notes_per_music = mat2str(self.nr_of_notes_per_music);
            STR_nr_of_notes_played = mat2str(self.nr_of_notes_played);
            STR_nr_of_notes_played_by_note_id = mat2str(self.nr_of_notes_played_by_noteid);
            values = STR_nr_of_piano_types + "," + self.frequency_samples + "," + self.frame_samples + "," + lower(self.data_type) + "," + self.overlap_ratio + "," + self.nr_of_sounds + "," + self.nr_of_sounds_as_musics + "," + self.nr_of_sounds_as_chrods_random + "," + self.nr_of_sounds_as_chrods_ucho + "," + self.nr_of_sounds_as_single_notes + "," + self.nr_frames + "," + self.nr_of_notes + "," + self.nr_frames_with_note + "," + self.nr_frames_without_note + "," + STR_nr_notes_per_frame + "," + STR_nr_of_notes_by_noteid + "," + STR_nr_of_notes_per_music + "," + STR_nr_of_notes_played + "," + STR_nr_of_notes_played_by_note_id;
            
        end
    end
end

