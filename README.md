# Project description #

**Note:** All the source code in this repository is derived from the work (dissertation): *Automatic Transcription of Music using Deep Learning Techniques* by Gil, Grilo, Reis and Domingues (2019).


This repository contains the source code for generating the dataset used for training and testing of the transcription system presented in the work mentioned above.
The source code of this transcription system is also available in [MEI-CM_ML_PitchDetection](https://bitbucket.org/matapatos/mei-cm_ml_pitchdetection/src/master/).


## Overview ##


This source code is based on the [MAPS Piano Dataset](http://www.tsi.telecom-paristech.fr/aao/en/2010/07/08/maps-database-a-piano-database-for-multipitch-estimation-and-automatic-transcription-of-music/).
It uses the Matlab tool for converting the raw musical *wav* and respective *txt* file into several *csv* files:

- *input.csv* - Contains the splitted frames of the musical piece.

- *output.csv* - Contains the labels, in this case a binary vector of 88 values per each frame.

- *local_metadata.csv* - "" Metadata regarding each musical piece. 

- *global_metadata.csv* - "" Metadata regarding the whole musical pieces. 

- *onset_output.csv* - **Optional** file that contains detected onsets based on the algorithm from [*A computational framework for sound segregation in music signals*](https://repositorio-aberto.up.pt/bitstream/10216/58570/1/000129328.pdf). 

- *possible_notes.csv* - **Optional** file that contains the possible notes per each frame based on the Possible Notes algorithm from [*Una Aproximación Genética a la Transcripción Automática de Música*](https://dialnet.unirioja.es/servlet/tesis?codigo=43169). 


## Possible dataset transformations ##

The resultant dataset can be generated with following possible transformations:

- Splitted frames (mandatory) - Split the musical pieces into frames of X samples.
- Use of overlapping (optional).
- Convert the signal into the frequency domain using the FFT (optional).
- Discard unrelevant frames, like silence ones (optional).
- Create a specific note dataset with a ratio rule that balances positive and negative frames (optional).

## Running project ##

Before running this project (*main.m* file), it is necessary to set up a few global variables in *main.m* file. These variables depends on the type of dataset that is necessary to be created: 1) training DS or 2) testing/evaluation DS.

### Training dataset example ###

For generating a training DS it is necessary to set the following variables accordingly:

```
IS_TO_REMOVE_ZERO_LINES = true;
DISCARD_FRAMES_WITHOUT_NOTES = true;
IS_TO_GENERATE_SPECIFIC_FRAMES_DATASETS = true;
IS_TO_GENERATE_FULL_DATASETS = false;
```

Apart from that is also necessary to specify the note MIDI number of the dataset that you want to create. For instance, if you want to create a dataset for the classifier responsible for detecting the note MIDI number 55 you need to set the following variable accordingly: 

```
DATASETS_OF_NOTES_TO_CREATE=[55];
```

To finish, an output path for the resultant dataset must be specified. For example:

```
DIRECTORY_CONTAINING_THE_MUSICS = '/my-pc/Desktop/Dataset/Training';
```
**Important:** The path specified in *DIRECTORY_CONTAINING_THE_MUSICS* must be an existent directory with a sub-directory denominated by *specific_frames_datasets*.

### Testing/evalution dataset example ###

For generating the testing DS the following variables must be set accordingly:

```
IS_TO_GENERATE_FULL_DATASETS = true;
IS_TO_REMOVE_ZERO_LINES = false;
DISCARD_FRAMES_WITHOUT_NOTES = false;
IS_TO_GENERATE_SPECIFIC_FRAMES_DATASETS = false;
```

Apart from that is also necessary to specify the output path:
```
DIRECTORY_CONTAINING_THE_MUSICS = '/my-pc/Desktop/Dataset/Testing';
```

**For more info** check the [dissertation](https://bitbucket.org/matapatos/mei-cm_ml_pitchdetection/raw/7a131130dd4fc6a888d4727d884a826cbcbb765d/Dissertation.pdf) or the following [PDF file](https://bitbucket.org/matapatos/mei-cm_ml_io_generator/raw/b3074a26190091f1a655ae3ba59abd70c607f423/Documentation.pdf).
