function possible_notes = GetPossibleNotes(fft_frames, threshold, frequency_samples, frame_samples, number_of_notes, first_note)
    nr_rows = size(fft_frames, 1);
    possible_notes = ones(nr_rows, number_of_notes);
    hz_per_bin = frequency_samples / frame_samples;
    last_note = first_note + number_of_notes - 1;
    for i = 1 :  size(fft_frames, 1)
        frame = fft_frames(i,:);
        notes = GetPossibleNotesFromFrame(frame, threshold, hz_per_bin, number_of_notes, first_note, last_note);
        if size(notes, 2) > 0
            possible_notes(i,:) = notes; % Fill notes into possible notes.
        end
    end
end

function notes = GetPossibleNotesFromFrame(frame, threshold, hz_per_bin, number_of_notes, min_note, max_note)
    picks = GetHighestPicks(frame, threshold);
    notes = GetPossibleNotesBasedOnPicks(picks, hz_per_bin, number_of_notes, min_note, max_note);
end

function possible_notes = GetPossibleNotesBasedOnPicks(picks, hz_per_bin, number_of_notes, min_note, max_note)
    nr_picks = size(picks, 2);
    if nr_picks == 0
        possible_notes = [];
    else
        possible_notes = zeros(1, number_of_notes);
        for i = 1 : nr_picks
            freq = picks(i) * hz_per_bin;
            possible_notes = GetPossibleNotesFromFrequency(possible_notes, freq, hz_per_bin, min_note, max_note);
        end
    end
end

function possible_notes = GetPossibleNotesFromFrequency(possible_notes, freq, hz_per_bin, min_note, max_note)
    min_note_freq = freq - hz_per_bin;
    max_note_freq = freq + hz_per_bin;
    
    first_note = FrequencyToNote(min_note_freq);
    last_note = FrequencyToNote(max_note_freq);
    [first_note, last_note] = GetOnlyValidNotes(first_note, last_note, min_note, max_note);
    if first_note ~= -1 && last_note ~= -1
        first_note = first_note - min_note + 1;
        last_note = last_note - min_note + 1;
        possible_notes(1,first_note:last_note) = 1; 
    end
end

function picks = GetHighestPicks(frame, threshold)
    max_freq = max(frame);
    result = max_freq - threshold;
    nr_samples = size(frame, 2);
    if result <= 0
        picks = [];
    else
        picks = [];
        for i = 1 : nr_samples
            val = frame(i);
            if val >= result
                next_index = size(picks,2) + 1;
                picks(1,next_index) = i;
            end
        end 
    end
end

function note_number = FrequencyToNote(f)
    note_number = round(12*log2(f/440) + 69);
end

function [first_note, last_note] = GetOnlyValidNotes(first_note, last_note, min_note_number, max_note_number)
    if first_note < min_note_number
       first_note = min_note_number;
       if last_note < min_note_number
            first_note = -1;
            last_note = -1;
       end
    end
    
    if last_note > max_note_number
        last_note = max_note_number;
        if first_note > max_note_number
            first_note = -1;
            last_note = -1;
        end
    end
end
