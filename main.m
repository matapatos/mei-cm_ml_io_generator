%Script for searching for .wav and associated .mid files in the specified folder and it's sub-folders
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Arguments order:
%overlap_ratio, is_to_remove_zero_lines, discard_frames_without_notes, is_to_convert_input_data_type_as_fft,
%is_to_squash_fft, is_to_generate_notes_datasets, datasets_of_notes_to_create, is_to_generate_full_datasets,
%is_to_generate_silence_dataset, is_to_generate_specific_frames_datasets, percentage_of_specific_notes_dataset, directory_containing_the_musics
function main(varargin)
    %Define globals
    global FREQUENCY_SAMPLES;
    global FRAME_SAMPLES;
    global SEP_TOKEN;
    global MAIN_OUTPUT_FOLDER;
    global INPUT_ROW_SEPARATOR;
    global OUTPUT_ROW_SEPARATOR;
    global OUTPUT_CSV_FILEPATH;
    global FIRST_NOTE_NUMBER;
    global NUMBER_OF_NOTES;
    global IS_TO_REMOVE_ZERO_LINES;
    global INPUT_CSV_FILEPATH;
    global LOCAL_METADATA_CSV_FILEPATH;
    global GLOBAL_METADATA_CSV_FILEPATH;
    global MAIN_FOLDER_NOTES_DATASETS;
    global IS_TO_GENERATE_NOTES_DATASETS;
    global DATASETS_OF_NOTES_TO_CREATE;
    global INPUT_ROW_SEPARATOR_FOR_FTT;
    global DISCARD_FRAMES_WITHOUT_NOTES;
    global PERCENTAGE_OF_SPECIFIC_NOTES_DATASET;
    global WARN_NO_TXT_FILE;
    global MAIN_FOLDER_SPECIFIC_FRAMES_DATASETS;
    global IS_TO_GENERATE_SPECIFIC_FRAMES_DATASETS;
    global DIRECTORY_CONTAINING_THE_MUSICS;
    global IS_TO_SQUASH_DATA_TYPE;
    global MAX_NR_OF_SIMULTANEOUSLY_NOTES;
    global ALGORITM_CONFIGS_USED_FILEPATH;
    global IS_TO_GENERATE_SILENCE_DATASET;
    global IS_TO_GENERATE_FULL_DATASETS;
    global OVERLAP_RATIO;
    global IS_TO_GENERATE_ONSET_DETECTION_OUTPUT;
    global FRAME_SAMPLES_ONSET_ALGORITHM;
    global OVERLAP_RATIO_ONSET_ALGORITHM;
    global ONSET_OUTPUT_FILEPATH;
    global RANDOM_SEED;
    global DATA_TYPE;
    global IS_TO_GENERATE_POSSIBLE_NOTES_OUTPUT;
    global POSSIBLE_NOTES_OUTPUT;
    global POSSIBLE_NOTES_THRESHOLD;
    global USE_BOX_COX_TO_SQUASH;
    global APPLY_WINDOWING;
    global APPLY_MEAN;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % ADAPTABLE Configurations
    %   -> Adapt the following configurations, in order to generate the
    %   expected datasets.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    OVERLAP_RATIO = 1; % Value must be between 0 (exclusive) and 1 (inclusive). If 1 is used, not overlap would be used, otherwise the overlap used is FRAME_SAMPLES * OVERLAP_RATIO
    %Flag for deciding if removes complete zero lines from the training samples
    IS_TO_REMOVE_ZERO_LINES = false;
    %Restrict selected frames
    DISCARD_FRAMES_WITHOUT_NOTES = false;
    %FFT as input/training data
    DATA_TYPE = "FFT";  %There are two main types: PURE, that is the pure signal, and FFT, that is abs(FFT(SIGNAL)).
    APPLY_WINDOWING = true;  % If true, it will windowing the signal before doing the FFT
    APPLY_MEAN = false;  % If true, a mean smooth will be applied to the fft signal.
    USE_BOX_COX_TO_SQUASH = false; % Only is used when IS_TO_SQUASH_DATA_TYPE == true && DATA_TYPE == "FFT"
    IS_TO_SQUASH_DATA_TYPE = false;
    %Generate notes datasets
    IS_TO_GENERATE_NOTES_DATASETS = false; %Enable/Disable Notes dataset creation
    DATASETS_OF_NOTES_TO_CREATE = []; % "all" for all the notes, or [55, 63, ...] containing the exact notes datasets to generate
    %Generate specific frames notes datasets
    IS_TO_GENERATE_FULL_DATASETS = true;
    IS_TO_GENERATE_SILENCE_DATASET = false;
    IS_TO_GENERATE_SPECIFIC_FRAMES_DATASETS = false; %Enable/Disable generation of specific_frames_datasets
    IS_TO_GENERATE_ONSET_DETECTION_OUTPUT = false;
    IS_TO_GENERATE_POSSIBLE_NOTES_OUTPUT = false;
    % This DS is different from the one of IS_TO_GENERATE_NOTES_DATASETS,
    % because it's frame specific. It selects only few frames from each music,
    % until reach a maximum percentage of PERCENTAGE_OF_SPECIFIC_NOTES_DATASET,
    % and NOT the FULL MUSIC.
    PERCENTAGE_OF_SPECIFIC_NOTES_DATASET = 20; %Number between 0 (inclusive) and 100 (exclusive). If 0 is used it will just create a full DS of that note, this can be used for the Silence DS specific dataset to generate the testing set.
    %Musics directory
    %DIRECTORY_CONTAINING_THE_MUSICS = strcat(MAIN_OUTPUT_FOLDER, '/../../Database/All/test');
    DIRECTORY_CONTAINING_THE_MUSICS = '/media/andre/AB4A-A1D0/Thesis/Database/70_musics/Test';
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % STATIC Configurations
    %   -> All the configurations in this sections are not intended to be
    %   changed, unless you know exactly what you are doing
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %General dataset specifications
    FIRST_NOTE_NUMBER = 21;
    NUMBER_OF_NOTES = 88;
    FREQUENCY_SAMPLES = 44100; %FS is in HZ
    FRAME_SAMPLES = 4096; %Number of samples/points per frame
    FRAME_SAMPLES_ONSET_ALGORITHM = 2048;
    OVERLAP_RATIO_ONSET_ALGORITHM = 441/2048; %1/4; % FRAME_SAMPLES_ONSET_ALGORITHM * OVERLAP_RATIO_ONSET_ALGORITHM = 512
    MAX_NR_OF_SIMULTANEOUSLY_NOTES = 50;
    %File separators
    SEP_TOKEN = -9;
    INPUT_ROW_SEPARATOR = GetRowSeparator(FRAME_SAMPLES);
    INPUT_ROW_SEPARATOR_FOR_FTT = GetRowSeparator(FRAME_SAMPLES/2);
    OUTPUT_ROW_SEPARATOR = GetRowSeparator(NUMBER_OF_NOTES);
    %Output folders and files
    MAIN_OUTPUT_FOLDER = [pwd filesep 'output'];
    INPUT_CSV_FILEPATH = [MAIN_OUTPUT_FOLDER filesep 'input.csv'];
    OUTPUT_CSV_FILEPATH = [MAIN_OUTPUT_FOLDER filesep 'output.csv'];
    LOCAL_METADATA_CSV_FILEPATH = [MAIN_OUTPUT_FOLDER filesep 'local_metadata.csv'];
    GLOBAL_METADATA_CSV_FILEPATH = [MAIN_OUTPUT_FOLDER filesep 'global_metadata.csv'];
    MAIN_FOLDER_NOTES_DATASETS = [MAIN_OUTPUT_FOLDER filesep 'notes'];
    MAIN_FOLDER_SPECIFIC_FRAMES_DATASETS = [MAIN_OUTPUT_FOLDER filesep 'specific_frames_datasets'];
    ALGORITM_CONFIGS_USED_FILEPATH = [MAIN_OUTPUT_FOLDER filesep 'configs.txt'];
    ONSET_OUTPUT_FILEPATH = [MAIN_OUTPUT_FOLDER filesep 'onset_output.csv'];
    POSSIBLE_NOTES_OUTPUT = [MAIN_OUTPUT_FOLDER filesep 'possible_notes.csv'];
    %Define ERRORS/WARNINGS Messages
    WARN_NO_TXT_FILE = 'No TXT file found associated with: ';
    %Ability to replicate datasets.
    RANDOM_SEED = 23;
    POSSIBLE_NOTES_THRESHOLD = 40;
    
    if nargin >= 1
        OVERLAP_RATIO = varargin{1};
    end
    if nargin >= 2
        IS_TO_REMOVE_ZERO_LINES = varargin{2};
    end
    if nargin >= 3
        DISCARD_FRAMES_WITHOUT_NOTES = varargin{3};
    end
    if nargin >= 4
        DATA_TYPE = varargin{4};
    end
    if nargin >= 5
        IS_TO_SQUASH_DATA_TYPE = varargin{5};
    end
    if nargin >= 6
        IS_TO_GENERATE_NOTES_DATASETS = varargin{6};
    end
    if nargin >= 7
        DATASETS_OF_NOTES_TO_CREATE = varargin{7};
    end
    if nargin >= 8
        IS_TO_GENERATE_FULL_DATASETS = varargin{8};
    end
    if nargin >= 8
        IS_TO_GENERATE_SILENCE_DATASET = varargin{8};
    end
    if nargin >= 10
        IS_TO_GENERATE_SPECIFIC_FRAMES_DATASETS = varargin{10};
    end
    if nargin >= 11
        PERCENTAGE_OF_SPECIFIC_NOTES_DATASET = varargin{11};
    end
    if nargin >= 12
        DIRECTORY_CONTAINING_THE_MUSICS = varargin{12};
    end
    if nargin >= 13
        IS_TO_GENERATE_ONSET_DETECTION_OUTPUT = varargin{13};
    end
    
    if ~exist(MAIN_OUTPUT_FOLDER, 'dir')
       mkdir(MAIN_OUTPUT_FOLDER);
    end
    GenerateDS()
end

function GenerateDS()
    global IS_TO_GENERATE_FULL_DATASETS;
    global GLOBAL_METADATA_CSV_FILEPATH;
    global LOCAL_METADATA_CSV_FILEPATH;
    global NUMBER_OF_NOTES;
    global SEP_TOKEN;
    global IS_TO_GENERATE_NOTES_DATASETS;
    global IS_TO_GENERATE_SPECIFIC_FRAMES_DATASETS;
    global MAIN_FOLDER_NOTES_DATASETS;
    global FIRST_NOTE_NUMBER;
    global DATASETS_OF_NOTES_TO_CREATE;
    global MAX_NR_OF_SIMULTANEOUSLY_NOTES;
    global ALGORITM_CONFIGS_USED_FILEPATH;
    global DIRECTORY_CONTAINING_THE_MUSICS;
    global IS_TO_GENERATE_SILENCE_DATASET;
    global IS_TO_GENERATE_ONSET_DETECTION_OUTPUT;
    global RANDOM_SEED;
    global FREQUENCY_SAMPLES;
    global FRAME_SAMPLES;
    global DATA_TYPE;
    global OVERLAP_RATIO;
    global IS_TO_GENERATE_POSSIBLE_NOTES_OUTPUT;
    global POSSIBLE_NOTES_OUTPUT;
    
    ConfigurationsInfo();
    
    if IS_TO_GENERATE_POSSIBLE_NOTES_OUTPUT
        WritePossibleNotesHeaders(POSSIBLE_NOTES_OUTPUT);
    end
    
    if IS_TO_GENERATE_FULL_DATASETS
        rng(RANDOM_SEED);
        meta = Metadata(GLOBAL_METADATA_CSV_FILEPATH, LOCAL_METADATA_CSV_FILEPATH, NUMBER_OF_NOTES, SEP_TOKEN, IS_TO_GENERATE_NOTES_DATASETS, IS_TO_GENERATE_SPECIFIC_FRAMES_DATASETS, MAIN_FOLDER_NOTES_DATASETS, FIRST_NOTE_NUMBER, DATASETS_OF_NOTES_TO_CREATE, MAX_NR_OF_SIMULTANEOUSLY_NOTES, ALGORITM_CONFIGS_USED_FILEPATH, FREQUENCY_SAMPLES, FRAME_SAMPLES, DATA_TYPE, OVERLAP_RATIO);
        meta = WriteInputAndOutputDataFromFolder(DIRECTORY_CONTAINING_THE_MUSICS, meta);
        meta = meta.WriteGlobalMetadata();
    end

    if IS_TO_GENERATE_SILENCE_DATASET
        rng(RANDOM_SEED + 1);
        GenerateSilenceDataset();
    end

    if IS_TO_GENERATE_SPECIFIC_FRAMES_DATASETS
        rng(RANDOM_SEED + 2);
        GenerateSpecificFramesDatasets();
    end
    
    if IS_TO_GENERATE_ONSET_DETECTION_OUTPUT
        rng(RANDOM_SEED + 3);
        GenerateOnsetOutput();
    end
end

function ConfigurationsInfo()
    global DIRECTORY_CONTAINING_THE_MUSICS;
    global IS_TO_REMOVE_ZERO_LINES;
    global DISCARD_FRAMES_WITHOUT_NOTES;
    global DATA_TYPE;
    global IS_TO_SQUASH_DATA_TYPE;
    global IS_TO_GENERATE_NOTES_DATASETS;
    global DATASETS_OF_NOTES_TO_CREATE;
    global IS_TO_GENERATE_SPECIFIC_FRAMES_DATASETS;
    global PERCENTAGE_OF_SPECIFIC_NOTES_DATASET;
    global IS_TO_GENERATE_SILENCE_DATASET;
    global IS_TO_GENERATE_FULL_DATASETS;
    global OVERLAP_RATIO;

    info = "Starting generating datasets...\n";
    info = info + "Configurations in use:\n";
    info = info + "\t-> Musics folder: '" + DIRECTORY_CONTAINING_THE_MUSICS + "'\n";
    info = info + "\t-> Overlap ratio: " + num2str(OVERLAP_RATIO) + "\n";
    info = info + "\t-> Is to generate full datasets: '" + IS_TO_GENERATE_FULL_DATASETS + "'\n";
    info = info + "\t-> Is to generate silence dataset: '" + IS_TO_GENERATE_SILENCE_DATASET + "'\n";
    info = info + "\t-> Is to remove zero lines: '" + IS_TO_REMOVE_ZERO_LINES + "'\n";
    info = info + "\t-> Discard frames without notes: '" + DISCARD_FRAMES_WITHOUT_NOTES + "'\n";
    info = info + "\t-> Data type: '" + lower(DATA_TYPE) + "'\n";
    if IS_TO_SQUASH_DATA_TYPE
       info = info + "\t\t-> Squash FFT input data: '" + IS_TO_SQUASH_DATA_TYPE + "'\n";
    else
        info = info + "\t\t-> Squash FFT input data: 'false'\n";
    end
    info = info + "\t-> Is to generate notes datasets: '" + IS_TO_GENERATE_NOTES_DATASETS + "'\n";
    info = info + "\t-> Is to generate specific frames datasets: '" + IS_TO_GENERATE_SPECIFIC_FRAMES_DATASETS + "'\n";
    if IS_TO_GENERATE_SPECIFIC_FRAMES_DATASETS
        info = info + "\t-> Percentage of specific frames datasets: '" + num2str(PERCENTAGE_OF_SPECIFIC_NOTES_DATASET) + "'\n";
    else
        info = info + "\t-> Percentage of specific frames datasets: ---\n";
    end
    
    if IS_TO_GENERATE_SPECIFIC_FRAMES_DATASETS || IS_TO_GENERATE_NOTES_DATASETS
        if isstring(DATASETS_OF_NOTES_TO_CREATE)
            info = info + "\t-> Notes datasets to create: '" + DATASETS_OF_NOTES_TO_CREATE + "'\n"; 
        else
            info = info + "\t-> Notes datasets to create: '" + num2str(DATASETS_OF_NOTES_TO_CREATE) + "'\n";
        end
    else
        info = info + "\t-> Notes datasets to create: ---\n";
    end
    
    sprintf(info)
end

function meta = WriteInputAndOutputDataFromFolder(folder, meta)
    global INPUT_CSV_FILEPATH;
    global OUTPUT_CSV_FILEPATH;
    
    progressbar('Full DS')
    all_files = GetAllWavAndTxtFilesFromFolder(folder, string([]));
    len_all_files = size(all_files, 1);
    for i = 1 : len_all_files
        wavFilepath = char(all_files(i, 1));
        txtFilepath = char(all_files(i, 2));
       
        [inputData, outputData, meta] = ProcessWavAndMidiFiles(meta, wavFilepath, txtFilepath);        
        WriteCSVData(INPUT_CSV_FILEPATH, inputData);
        WriteCSVData(OUTPUT_CSV_FILEPATH, outputData);
        %Update progress bar
        progressbar(i/len_all_files)
    end
end

function [inputCSVData, outputCSVData, meta] = ProcessWavAndMidiFiles(meta, wavFilepath, txtFilepath)
    global IS_TO_GENERATE_NOTES_DATASETS;
    
    [inputData, outputData, nr_of_notes_per_music] = GetFilteredInputAndOutputData(wavFilepath, txtFilepath);    
    [inputCSVData, outputCSVData] = AppendMusicSeparator(inputData, outputData);
    
    [meta, localMeta] = meta.AppendLocalMetadataOfMusic(wavFilepath, inputData, outputData, nr_of_notes_per_music);
    if IS_TO_GENERATE_NOTES_DATASETS
       GenerateNotesDatasets(inputCSVData, outputCSVData, localMeta);
    end
end

function [inputData, outputData, nr_of_notes_per_music] = GetFilteredInputAndOutputData(wavFilepath, txtFilepath)
    global NUMBER_OF_NOTES;
    global FRAME_SAMPLES;
    global FIRST_NOTE_NUMBER;
    global IS_TO_REMOVE_ZERO_LINES;
    global DISCARD_FRAMES_WITHOUT_NOTES;
    global DATA_TYPE;
    global OVERLAP_RATIO;
    global IS_TO_GENERATE_POSSIBLE_NOTES_OUTPUT;
    global POSSIBLE_NOTES_THRESHOLD;
    global FREQUENCY_SAMPLES;
    global POSSIBLE_NOTES_OUTPUT;

    %Get input CSV Data from audio file
    [inputData, FreqPerSec] = GetInputCSVDataFromAudio(wavFilepath, FRAME_SAMPLES, OVERLAP_RATIO);
    nr_input_rows = size(inputData, 1);
    [outputData, nr_of_notes_per_music] = GetOutputCSVDataFromTXT(txtFilepath, FreqPerSec, nr_input_rows, NUMBER_OF_NOTES, FRAME_SAMPLES, FIRST_NOTE_NUMBER, OVERLAP_RATIO);
    
    
    if DISCARD_FRAMES_WITHOUT_NOTES
        [inputData, outputData] = DiscardFramesWithoutNotes(inputData, outputData);
    end
    
    if IS_TO_REMOVE_ZERO_LINES
        [inputData, outputData] = RemoveZeroLines(inputData, outputData);
    end
    
    if lower(DATA_TYPE) == "fft"
        inputData = ConvertInputDataAsFFT(inputData);
        if IS_TO_GENERATE_POSSIBLE_NOTES_OUTPUT
           possible_notes = GetPossibleNotes(inputData, POSSIBLE_NOTES_THRESHOLD, FREQUENCY_SAMPLES, FRAME_SAMPLES, NUMBER_OF_NOTES, FIRST_NOTE_NUMBER);
           WritePossibleNotes(POSSIBLE_NOTES_OUTPUT, wavFilepath, possible_notes);
        end
    end
end

function [inputCSVData, outputCSVData] = AppendMusicSeparator(inputData, outputData)
    global INPUT_ROW_SEPARATOR;
    global OUTPUT_ROW_SEPARATOR;
    global DATA_TYPE;
    global INPUT_ROW_SEPARATOR_FOR_FTT;
    
    if lower(DATA_TYPE) == "fft"
        input_row_separator = INPUT_ROW_SEPARATOR_FOR_FTT;
    else        
        input_row_separator = INPUT_ROW_SEPARATOR;
    end
    
    inputCSVData = vertcat(inputData, input_row_separator);
    outputCSVData = vertcat(outputData, OUTPUT_ROW_SEPARATOR); %Add empty row
end

function [finalInputData, finalOutputData] = DiscardFramesWithoutNotes(inputData, outputData)
    
    %Create discard condition
    discard_condition = all(outputData(:,:) == 0, 2); %For all the frames without at least one note it will return true and false otherwise
    %Discard/Remove frames
    inputData(discard_condition, :) = [];
    outputData(discard_condition, :) = [];
    
    finalInputData = inputData;
    finalOutputData = outputData;
end

%Box-Cox implementation based from: https://danielsdiscoveries.wordpress.com/2017/09/29/spectrogram-input-normalisation-for-neural-networks/
function inputFFTData = ConvertInputDataAsFFT(inputData)
    global FRAME_SAMPLES;
    global IS_TO_SQUASH_DATA_TYPE;
    global USE_BOX_COX_TO_SQUASH;
    global APPLY_WINDOWING;
    global APPLY_MEAN;
    
    nr_rows = size(inputData, 1);
    nr_cols = FRAME_SAMPLES / 2;
    inputFFTData = zeros(nr_rows, nr_cols);
    window = hamming(4096)';
    for i = 1 : nr_rows
        if APPLY_WINDOWING
            inputData(i,:) = inputData(i,:) .* window;
        end
        fftResult = abs(fft(inputData(i,:)));
        if IS_TO_SQUASH_DATA_TYPE
            if USE_BOX_COX_TO_SQUASH
                y1 = 0.043;
                y2 = 10^-7;
                
                for j = 1 : size(fftResult, 2)
                    val = fftResult(j);
                    if val == 0
                        fftResult(j) = log(val + y2);
                    else
                        fftResult(j) = ((val + y2)^y1 - 1)/y1;
                    end
                end
            else
                fftResult(fftResult == 0) = 0.0001;
                fftResult = log(fftResult)/log(FRAME_SAMPLES);
            end
        end
        fftResult = fftResult(1:nr_cols);
        if APPLY_MEAN
            mean_fft_result = zeros(1, nr_cols);
            mean_fft_result(1) = sum(fftResult(1:3))/3;
            mean_fft_result(2) = sum(fftResult(1:4))/4;
            mean_fft_result(end) = sum(fftResult(end-2:end))/3;
            mean_fft_result(end-1) = sum(fftResult(end-3:end))/4;
            for j = 3 : nr_cols - 2
                mean_fft_result(j) = sum(fftResult(j-2:j+2))/5;
            end
            fftResult = mean_fft_result;
        end
        inputFFTData(i,:) = fftResult;
    end
end

function GenerateNotesDatasets(inputData, outputData, localMeta)
    global FIRST_NOTE_NUMBER;
    global DATASETS_OF_NOTES_TO_CREATE;
    
    notes = localMeta.l_nr_of_notes_per_music;
    startNoteIndex = FIRST_NOTE_NUMBER - 1;
    for i = 1 : size(notes, 2)
        n = notes(i);
        if n > 0
            noteNumber = i + startNoteIndex;
            if (isstring(DATASETS_OF_NOTES_TO_CREATE) && DATASETS_OF_NOTES_TO_CREATE == "all") || ismember(noteNumber, DATASETS_OF_NOTES_TO_CREATE)
                CreateNotesDataset(noteNumber, inputData, outputData);
            end
        end
    end
end

function CreateNotesDataset(noteNumber, inputData, outputData)
    global MAIN_FOLDER_NOTES_DATASETS;
    
    inputFilepath = strcat(MAIN_FOLDER_NOTES_DATASETS, "/" + num2str(noteNumber) + "_train.csv");
    outputFilepath = strcat(MAIN_FOLDER_NOTES_DATASETS, "/" + num2str(noteNumber) + "_output.csv");

    WriteCSVData(inputFilepath, inputData);
    WriteCSVData(outputFilepath, outputData);
end

function [inputCSVData, outputCSVData] = RemoveZeroLines(inputCSVData, outputCSVData)
    t_input_data = inputCSVData'; %Convert frames to columns
    t_output_data = outputCSVData';
    
    %Create deletion condition
    t_TFall = all(t_input_data(:,:) == 0); %For all the zero frames it will return true and false otherwise
    %Delete zero frames data
    t_output_data(:, t_TFall) = [];
    t_input_data(:, t_TFall) = []; %Remove zero frames
    
    inputCSVData = t_input_data';
    outputCSVData = t_output_data';
end

function GenerateSilenceDataset()
    % This function contains a known bug when creating the DS. Still needs
    % to be fixed!

    global SEP_TOKEN;
    global DIRECTORY_CONTAINING_THE_MUSICS;
    global FIRST_NOTE_NUMBER;
    global NUMBER_OF_NOTES;
    global MAIN_FOLDER_SPECIFIC_FRAMES_DATASETS;
    global MAX_NR_OF_SIMULTANEOUSLY_NOTES;
    global DISCARD_FRAMES_WITHOUT_NOTES;
    global IS_TO_REMOVE_ZERO_LINES;
    global FREQUENCY_SAMPLES;
    global FRAME_SAMPLES;
    global DATA_TYPE;
    global OVERLAP_RATIO;
    
    %Being able retrieve silence frames
    original_discard_frames_without_notes = DISCARD_FRAMES_WITHOUT_NOTES;
    DISCARD_FRAMES_WITHOUT_NOTES = false;
    original_is_to_remove_zero_lines = IS_TO_REMOVE_ZERO_LINES;
    IS_TO_REMOVE_ZERO_LINES = false;
    
    %Prepare variables
    all_files = GetAllWavAndTxtFilesFromFolder(DIRECTORY_CONTAINING_THE_MUSICS, string([]));
    len_all_files = size(all_files, 1);
    len_progressbar = len_all_files;
    progressbar('Silence DS')
    silence_input_csv_filepath = MAIN_FOLDER_SPECIFIC_FRAMES_DATASETS + "/silence_input.csv";
    silence_output_csv_filepath = MAIN_FOLDER_SPECIFIC_FRAMES_DATASETS + "/silence_output.csv";
    silence_global_csv_metadata = MAIN_FOLDER_SPECIFIC_FRAMES_DATASETS + "/silence_global_metadata.csv";
    silence_local_csv_metadata = MAIN_FOLDER_SPECIFIC_FRAMES_DATASETS + "/silence_local_metadata.csv";
    silence_config_filepath = MAIN_FOLDER_SPECIFIC_FRAMES_DATASETS + "/silence_configs.txt";
    silence_meta = Metadata(silence_global_csv_metadata, silence_local_csv_metadata, NUMBER_OF_NOTES, SEP_TOKEN, false, false, MAIN_FOLDER_SPECIFIC_FRAMES_DATASETS, FIRST_NOTE_NUMBER, [], MAX_NR_OF_SIMULTANEOUSLY_NOTES, silence_config_filepath, FREQUENCY_SAMPLES, FRAME_SAMPLES, DATA_TYPE, OVERLAP_RATIO);
    global_nr_frames_without_note = 0;
    global_nr_frames_with_note = 0;
    missing_frames_without_notes = 0;
    
    for j = 1 : len_all_files
        wav_filepath = char(all_files(j, 1));
        txt_filepath = char(all_files(j, 2));

        [input_data, output_data, nr_of_notes_per_music] = GetFilteredInputAndOutputData(wav_filepath, txt_filepath);
        [new_input_data, new_output_data, global_nr_frames_with_note, global_nr_frames_without_note, missing_frames_without_notes] = GetSpecificFramesOfMusicByNote(wav_filepath, "silence", input_data, output_data, global_nr_frames_with_note, global_nr_frames_without_note, missing_frames_without_notes);
        if size(new_input_data, 1) == 0
            message = "No specific frames available from '" + wav_filepath + "'";
            sprintf(message);
        else                
            [input_CSV_data, output_CSV_data] = AppendMusicSeparator(new_input_data, new_output_data);
            [silence_meta, local_meta] = silence_meta.AppendLocalMetadataOfMusic(wav_filepath, new_input_data, new_output_data, nr_of_notes_per_music);

            WriteCSVData(silence_input_csv_filepath, input_CSV_data);
            WriteCSVData(silence_output_csv_filepath, output_CSV_data);
        end

        %Update progressbar
        progressbar(j/len_progressbar)
    end

    InfoOfSpecificFramesDataset("silence", global_nr_frames_with_note, global_nr_frames_without_note);
    silence_meta.WriteGlobalMetadata();
    DISCARD_FRAMES_WITHOUT_NOTES = original_discard_frames_without_notes;
    IS_TO_REMOVE_ZERO_LINES = original_is_to_remove_zero_lines;
end

function GenerateSpecificFramesDatasets()
    global DATASETS_OF_NOTES_TO_CREATE;
    global FIRST_NOTE_NUMBER;
    global NUMBER_OF_NOTES;
    global SEP_TOKEN;
    global MAIN_FOLDER_SPECIFIC_FRAMES_DATASETS;
    global DIRECTORY_CONTAINING_THE_MUSICS;
    global MAX_NR_OF_SIMULTANEOUSLY_NOTES;
    global FRAME_SAMPLES;
    global FREQUENCY_SAMPLES;
    global OVERLAP_RATIO;
    global DATA_TYPE;
    
    specific_frames_notes = DATASETS_OF_NOTES_TO_CREATE;
    if isstring(DATASETS_OF_NOTES_TO_CREATE) && DATASETS_OF_NOTES_TO_CREATE == "all"
        specific_frames_notes = zeros(1, NUMBER_OF_NOTES);
        for i = 1 : NUMBER_OF_NOTES
           specific_frames_notes(i) = FIRST_NOTE_NUMBER + i - 1;
        end
    end
    
    all_files = GetAllWavAndTxtFilesFromFolder(DIRECTORY_CONTAINING_THE_MUSICS, string([]));
    len_specific_frames_notes= size(specific_frames_notes, 2);
    len_all_files = size(all_files, 1);
    len_progressbar = len_specific_frames_notes * len_all_files;
    progressbar('Specific Frames DS')
    for i = 1 : len_specific_frames_notes 
        note = specific_frames_notes(i);
        note_input_csv_filepath = MAIN_FOLDER_SPECIFIC_FRAMES_DATASETS + "/" + num2str(note) + "_input.csv";
        note_output_csv_filepath = MAIN_FOLDER_SPECIFIC_FRAMES_DATASETS + "/" + num2str(note) + "_output.csv";
        specific_frames_global_csv_metadata = MAIN_FOLDER_SPECIFIC_FRAMES_DATASETS + "/" + num2str(note) + "_global_metadata.csv";
        specific_frames_local_csv_metadata = MAIN_FOLDER_SPECIFIC_FRAMES_DATASETS + "/" + num2str(note) + "_local_metadata.csv";
        specific_frames_config_filepath = MAIN_FOLDER_SPECIFIC_FRAMES_DATASETS + "/" + num2str(note) + "_configs.txt";
        specific_frames_meta = Metadata(specific_frames_global_csv_metadata, specific_frames_local_csv_metadata, NUMBER_OF_NOTES, SEP_TOKEN, false, false, MAIN_FOLDER_SPECIFIC_FRAMES_DATASETS, FIRST_NOTE_NUMBER, [], MAX_NR_OF_SIMULTANEOUSLY_NOTES, specific_frames_config_filepath, FREQUENCY_SAMPLES, FRAME_SAMPLES, DATA_TYPE, OVERLAP_RATIO);
        global_nr_frames_without_note = 0;
        global_nr_frames_with_note = 0;
        missing_frames_without_notes = 0;
        
        for j = 1 : len_all_files
            wav_filepath = char(all_files(j, 1));
            txt_filepath = char(all_files(j, 2));
          
            [input_data, output_data, nr_of_notes_per_music] = GetFilteredInputAndOutputData(wav_filepath, txt_filepath);
            [new_input_data, new_output_data, global_nr_frames_with_note, global_nr_frames_without_note, missing_frames_without_notes] = GetSpecificFramesOfMusicByNote(wav_filepath, note, input_data, output_data, global_nr_frames_with_note, global_nr_frames_without_note, missing_frames_without_notes); %TODO Ensure if frames without note are missing, ensure here that the next music it would return more frames
            if size(new_input_data, 1) == 0
                message = "No specific frames available from '" + wav_filepath + "'";
                sprintf(message);
            else                
                [input_CSV_data, output_CSV_data] = AppendMusicSeparator(new_input_data, new_output_data);
                [specific_frames_meta, local_meta] = specific_frames_meta.AppendLocalMetadataOfMusic(wav_filepath, new_input_data, new_output_data, nr_of_notes_per_music);
                
                WriteCSVData(note_input_csv_filepath, input_CSV_data);
                WriteCSVData(note_output_csv_filepath, output_CSV_data);
            end
            
            %Update progressbar
            index = i-1;
            if index >= 1
               index = index * len_all_files;
            end
            progressbar((index+j)/len_progressbar)
        end
        
        InfoOfSpecificFramesDataset(note, global_nr_frames_with_note, global_nr_frames_without_note);
        specific_frames_meta.WriteGlobalMetadata();
    end
end

function InfoOfSpecificFramesDataset(note, nr_frames_with_note, nr_frames_without_note)
    global PERCENTAGE_OF_SPECIFIC_NOTES_DATASET;
    
    real_percentage = 0;
    if nr_frames_with_note > 0
        real_percentage = 100 - round((nr_frames_without_note * PERCENTAGE_OF_SPECIFIC_NOTES_DATASET)/nr_frames_with_note);
    end
    info = "----- Specific frame dataset -----\n";
    if isstring(note)
        info = info + "Note: " + note + "\n";
    else
        info = info + "Note: " + num2str(note) + "\n";
    end
    note_str = note;
    if ~isstring(note_str)
        note_str = num2str(note);
    end
    info = info + "Expected percentage of " + note_str + ": " + num2str(PERCENTAGE_OF_SPECIFIC_NOTES_DATASET) + "\n";
    info = info + "Real percentage: " + num2str(real_percentage) + "\n";
    info = info + "Number of frames with " + note_str + ": " + num2str(nr_frames_with_note) + "\n";
    info = info + "Number of frames without " + note_str + ": " + num2str(nr_frames_without_note) + "\n";
    info = info + "----------------------------------\n";
    
    sprintf(info)
end

function [frames_indexes_with_note, frames_indexes_without_note] = GetSeparatedIndexesOfFramesByNoteId(note, output_data)
    global FIRST_NOTE_NUMBER;

    frames_indexes_with_note = [];
    frames_indexes_without_note = [];
    
    if isstring(note)
        total = sum(output_data, 2);
        for i = 1 : size(total,1)
           val = total(i);
           if val == 0 % Is a silence frame
               frames_indexes_with_note(end + 1) = i;
           else
               frames_indexes_without_note(end + 1) = i;
           end
        end
    else
        note_index = note - FIRST_NOTE_NUMBER + 1;
    
        for i = 1 : size(output_data,1)
            hasNote = output_data(i, note_index);
            if hasNote == 0 %Doesn't contains the note
                frames_indexes_without_note(end + 1) = i;
            else
                frames_indexes_with_note(end + 1) = i;
            end
        end
    end
end

function [new_input_data, new_output_data, global_nr_frames_with_note, global_nr_frames_without_note, missing_frames_without_notes] = GetSpecificFramesOfMusicByNote(wav_filepath, note, input_data, output_data, global_nr_frames_with_note, global_nr_frames_without_note, missing_frames_without_notes)
    global PERCENTAGE_OF_SPECIFIC_NOTES_DATASET;
    new_input_data = [];
    new_output_data = [];
    
    [frames_indexes_with_note, frames_indexes_without_note] = GetSeparatedIndexesOfFramesByNoteId(note, output_data);
    num_frames_with_note = size(frames_indexes_with_note, 2);
    if num_frames_with_note == 0 && missing_frames_without_notes == 0
        note_str = note;
        if ~isstring(note_str)
            note_str = num2str(note);
        end
        warn_msg = "WARN: The following sound '" + wav_filepath + "' doesnt contain any frame with the note number " + note_str + " so it will be discarded all the data";
        sprintf(warn_msg);
        return; 
    end
    
    %Allocate necessary array space
    if PERCENTAGE_OF_SPECIFIC_NOTES_DATASET == 0
        max_nr_frames_without_note = size(frames_indexes_without_note, 2);
        new_input_data = zeros(num_frames_with_note + max_nr_frames_without_note, size(input_data, 2));
        new_output_data = zeros(num_frames_with_note + max_nr_frames_without_note, size(output_data, 2));
    else
        percentage_of_frames_without_note = 100 - PERCENTAGE_OF_SPECIFIC_NOTES_DATASET;
        max_nr_frames_without_note = round((num_frames_with_note * percentage_of_frames_without_note) / PERCENTAGE_OF_SPECIFIC_NOTES_DATASET);
        max_missing_frames = min(missing_frames_without_notes, size(input_data, 1)); % To make sure that it will not try to have more rows than the available music has
        max_nr_frames_without_note = max_nr_frames_without_note + max_missing_frames;
        new_input_data = zeros(num_frames_with_note + max_nr_frames_without_note, size(input_data, 2));
        new_output_data = zeros(num_frames_with_note + max_nr_frames_without_note, size(output_data, 2));
    end
    %Append frames with note
    for i = 1 : num_frames_with_note
        oldIndex = frames_indexes_with_note(i);
        new_input_data(i,:) = input_data(oldIndex, :);
        new_output_data(i,:) = output_data(oldIndex, :);
    end
    global_nr_frames_with_note = global_nr_frames_with_note + num_frames_with_note;
    
    %Append frames without note
    num_frames_without_note = size(frames_indexes_without_note, 2);
    initial_new_input_data_size = size(new_input_data, 1);
    start_index = num_frames_with_note + 1;
    for i = start_index : initial_new_input_data_size
        if num_frames_without_note <= 0
            new_input_data(i:end,:) = [];
            new_output_data(i:end,:) = [];
            break;
        else
           %Choose random index from the random frames available
           random_frame_index = randi(num_frames_without_note);
           real_frame_index = frames_indexes_without_note(random_frame_index);
           %Insert random frame in the data itself
           new_input_data(i,:) = input_data(real_frame_index, :);
           new_output_data(i,:) = output_data(real_frame_index, :);
           %Remove that frame from the options of frames
           frames_indexes_without_note(random_frame_index) = []; %Remove this index from the random, to don't repeat data
           
           %Update value of numFramesWithoutNote
           num_frames_without_note = size(frames_indexes_without_note, 2); 
        end
    end
    global_nr_frames_without_note = global_nr_frames_without_note + size(new_input_data, 1) - num_frames_with_note;    
    missing_frames_without_notes = initial_new_input_data_size - size(new_input_data, 1);
end

function WriteCSVData(filepath, data)
    dlmwrite(filepath, data, 'delimiter',',','-append');
end

function WriteOnsetData(filepath, wav_filepath, onsets)
    [pathstr,name,ext] = fileparts(wav_filepath);
    % Filename, onsets
    fid = fopen(filepath, 'a');
    fprintf(fid, char(name));
    fprintf(fid, ",");
    fprintf(fid, mat2str(onsets) + "\n");
    fclose(fid);
end

function WritePossibleNotesHeaders(filepath)
    headers = "filename,possible_notes\n";
    fid = fopen(filepath, 'a');
    fprintf(fid, headers);
    fclose(fid);
end

function WritePossibleNotes(filepath, wav_filepath, possible_notes)
    [pathstr,name,ext] = fileparts(wav_filepath);

    fid = fopen(filepath, 'a');
    fprintf(fid, char(name));
    fprintf(fid, ",");
    fprintf(fid, mat2str(possible_notes) + "\n");
    fclose(fid);
end

function WriteOnsetHeaders(filepath)
    headers = "filename,onsets\n";
    fid = fopen(filepath, 'a');
    fprintf(fid, headers);
    fclose(fid);
end

function sep = GetRowSeparator(numColumns)
    global SEP_TOKEN;
    
    sep = zeros(1, numColumns);
    for i = 1 : numColumns
       sep(1, i) = SEP_TOKEN;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% All the code bellow is based on
% "A Computational Framework for Sound Segregation in Music Signals"
% Section 3.5.3 Onset Detection Algorithm
% That is available on:
%   https://repositorio-aberto.up.pt/bitstream/10216/58570/1/000129328.pdf
% 
% 
% It's important to mention that this implementation was not possible
% without Prof. Gustavo Reis.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function GenerateOnsetOutput()
    global DIRECTORY_CONTAINING_THE_MUSICS;
    global FRAME_SAMPLES_ONSET_ALGORITHM;
    global OVERLAP_RATIO_ONSET_ALGORITHM;
    global ONSET_OUTPUT_FILEPATH;
    
    %progressbar('Onset output DS')
    all_files = GetAllWavAndTxtFilesFromFolder(DIRECTORY_CONTAINING_THE_MUSICS, string([]));
    len_all_files = size(all_files, 1);
    WriteOnsetHeaders(ONSET_OUTPUT_FILEPATH);
    window = hamming(FRAME_SAMPLES_ONSET_ALGORITHM)';
    for i = 1 : len_all_files
        wav_filepath = char(all_files(i, 1));
       
        [input_data, freq_per_sec] = GetInputCSVDataFromAudio(wav_filepath, FRAME_SAMPLES_ONSET_ALGORITHM, OVERLAP_RATIO_ONSET_ALGORITHM);
        
        nr_frames = size(input_data, 1);
        processed_input = zeros(1, nr_frames);
        window = hamming(size(input_data, 2))';
        if nr_frames > 0
            previous_frame = GetHammedFrame(input_data(1,:), window); % Get current frame
            for j = 2 : nr_frames
                current_frame = GetHammedFrame(input_data(j,:), window); % Get current frame

                processed_input(j) = SpectralFluxDistance(current_frame, previous_frame);
                previous_frame = current_frame;
            end
        end
        % For smoothing Matlab has already the following function:
        processed_input = filtfilt([0.1173 0.2347 0.1174],[1 -0.8252 0.2946], processed_input);
        % Otherwise we would need to use the following two functions:
        %processed_input = ForwardSmoothing(processed_input); % Forward smoothing
        %processed_input = ReverseSmoothing(processed_input); % Backward/Reverse smoothing
        
        onsets = GetOnsets(processed_input);
        WriteOnsetData(ONSET_OUTPUT_FILEPATH, wav_filepath, onsets);
        %Update progress bar
        progressbar(i/len_all_files)
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OPTIMIZATION of abs(fft) proof:
%
% Verifica a seguinte equacao:
% (a+b) * (a-b) = a^2 - b^2, certo?
% Agora repara no seguinte, um complexo tem duas componentes: a real e a imaginária
% c = (a + bi)
% sendo o conjugado do mesmo complexo o seguinte: 
% conj(c) = (a - bi)
% entao se multiplicarmos o c pelo seu conjugado temos:
% c * conj(c) = (a+b) * (a-bi) = a^2 - b^2*i^2
% como i^2 é igual a -1, fica:
% a^2 + b^2
% já o valor absoluto de um complexo é dado pela seguinte fórmula:
% |c| = square_root(a^2 + b^2)
% isto é: um é o quadrado do outro quando se quer calcular a magnitude, usa-se o logaritmo. E neste caso podes fazer das seguintes formas:
% 20 * log10(|c|) ou então 10 * log10(c * conj(c))
% O segundo logaritmo só é multiplicado por 10 e não por 20, porque:
% c * conj(c) = |c^2|
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function frame = GetHammedFrame(orig_signal, window)
    frame = orig_signal .* window; %Hamming of frame
    frame = fft(frame);
    frame = frame(1,1:end/2); % Remove the half of the FFT
    frame = frame .* conj(frame); % This line is an optimization of abs(FFT). Because abs(...) uses the root square that is an expensive operation.
    
    % Why not using 10 * log10 then??
    %frame(frame == 0) = 0.0001; % Remove 0s from log otherwise it returns NaN
    %frame = 10 * log10(frame); % We use 10 * log10 here because we use the optimization of the conj(frame) in line 809. If we used abs(fft) it would be 20 * log10(frame)
end

function out = ForwardSmoothing(in)
    nr_rows = size(in, 1);
    out = zeros(nr_rows, 1);
    for i = 3 : nr_rows
        out(i) = (0.1173 * in(i) + 0.2347 * in(i-1) + 0.1174 * in(i-2)) - (- 0.8252 * out(i-1) + 0.2946 * out(i-2));
    end
end

function out = ReverseSmoothing(in)
    nr_rows = size(in, 1);
    out = zeros(nr_rows, 1);
    for i = nr_rows-2:-1:1 % Reverse for loop
        out(i) = (0.1173 * in(i) + 0.2347 * in(i+1) + 0.1174 * in(i+2)) - (- 0.8252 * out(i+1) + 0.2946 * out(i+2));
    end
end

function onsets = GetOnsets(frames)
    % For each frame:
    %   Check if the following conditions exists in order to be considered an onset
    %       1 - SF (k) ∀k : n − w ≤ k ≤ n + w
    %       2 - SF (k)/(mw+w+1) × thres + δ
    
    w = 6; % size of the window used to find a local maximum
    m = 4; % multiplier so that the mean is calculated over a large range before the peak
    thres = 1; % threshold relative to the local mean
    delta = 10^-20; % residual value to avoid false detections on silence regions of the signal.

    nr_frames = size(frames, 2);
    onsets = zeros(1, nr_frames);
    for i = m * w + 1: nr_frames - w % It's plus one because Matlab is first index is 1. And is 'nr_frames - w' because in matlab the end index on a for loop is inclusive
        if IsOnset(frames, i, w, m, thres, delta)
            onsets(i) = 1;
        end
    end
    
    onsets = ConvertOnsetsToTime(onsets);
end

function onsets = ConvertOnsetsToTime(frames)
    global FREQUENCY_SAMPLES;
    global FRAME_SAMPLES_ONSET_ALGORITHM;
    global OVERLAP_RATIO_ONSET_ALGORITHM;

    hopsize = FRAME_SAMPLES_ONSET_ALGORITHM * OVERLAP_RATIO_ONSET_ALGORITHM;
    onsets = [];
    for i = 1 : size(frames, 2)
        val = frames(i);
        if val == 1
            index = i - 1; % It's minus one because matlab index doesn't start at 0
            seconds = (index * hopsize) / FREQUENCY_SAMPLES;
            next_onset_index = size(onsets, 2) + 1;
            onsets(1, next_onset_index) = seconds;
        end
    end
end

function is_onset = IsOnset(frames, index, w, m, thres, delta)
    is_onset = false;
    if IsCondition1(frames, index, w) && IsCondition2(frames, index, w, m, thres, delta)
        is_onset = true; 
    end
end

function is_valid = IsCondition1(frames, index, w)
    is_valid = false;
    if frames(index) >= max(frames(index - w: index + w))
        is_valid = true;
    end
end

function is_valid = IsCondition2(frames, index, w, m, thres, delta)
    is_valid = false;
    total = sum(frames(index - m*w:index + w));
    total = (total/(m * w + w + 1)) * thres + delta;
    if frames(index) > total
       is_valid = true; 
    end
end

function result = SpectralFluxDistance(current_frame, previous_frame)
    result = HalfWaveRectifier(current_frame - previous_frame);
    result = sum(result);
end

function result = HalfWaveRectifier(result)
    result(result < 0) = 0; % Assign 0 to values less than 0
end
