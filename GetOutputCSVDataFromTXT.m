function [final_data, nr_of_notes_per_music] = GetOutputCSVDataFromTXT(filepath, FreqPerSec, nr_of_final_rows, nr_of_notes, frame_samples, first_note_number, overlap_ratio)
    data = tdfread(filepath, '\t');
    onsetTime = data.OnsetTime;
    offsetTime = data.OffsetTime;
    midiPitch = data.MidiPitch; %Note number

    onsetTime = ConvertSecondsToSamples(onsetTime, FreqPerSec);
    offsetTime = ConvertSecondsToSamples(offsetTime, FreqPerSec);
    final_data = [onsetTime, offsetTime, midiPitch];
    nr_of_notes_per_music = GetNumberOfNotesFromData(midiPitch, nr_of_notes, first_note_number);
    final_data = TransformToFinalOutputData(final_data, nr_of_final_rows, nr_of_notes, frame_samples, first_note_number, overlap_ratio);
end

function nr_of_notes_per_music = GetNumberOfNotesFromData(midiPitch, nr_of_notes, first_note_number)
    nr_of_notes_per_music = zeros(1, nr_of_notes);
    for i = 1 : size(midiPitch, 1)
        note_number = midiPitch(i);
        note_index = GetNoteIndexFromNoteNumber(note_number, first_note_number);
        nr_of_notes_per_music(1, note_index) = nr_of_notes_per_music(1, note_index) + 1;
    end
end

function final_data = TransformToFinalOutputData(data, nr_of_final_rows, nr_of_notes, frame_samples, first_note_number, overlap_ratio)
    final_data = zeros(nr_of_final_rows, nr_of_notes);
    for i = 1 : size(data, 1)
        onsettime = data(i, 1);
        offsettime = data(i,2);
        [start_index, end_index] = GetIndexesBetweenSamples(onsettime, offsettime, frame_samples, overlap_ratio);
        
        note_number = data(i,3);
        note_index = GetNoteIndexFromNoteNumber(note_number, first_note_number);
        for x = start_index : end_index
           final_data(x, note_index) = 1;
        end
    end
end

function note_index = GetNoteIndexFromNoteNumber(note_number, first_note_number)
    note_index = note_number - first_note_number + 1; % It's a plus one because in matlab the first index is 1 instead of 0
    if note_index <= 0
        error('Invalid note index! Used note number: %d\nFirst note number: %d', note_number, first_note_number);
    end
end

function [start_index, end_index] = GetIndexesBetweenSamples(min_sample, max_sample, frame_samples, overlap_ratio)
    %start_index = GetFirstIndexFromSample(min_sample, frame_samples, overlap_ratio);
    %end_index = GetLastIndexFromSample(max_sample, frame_samples, overlap_ratio);
    
    start_index = GetFirstBestIndexFromSample(min_sample, frame_samples, overlap_ratio);
    end_index = GetLastBestIndexFromSample(max_sample, frame_samples, overlap_ratio);
end

function index = GetFirstIndexFromSample(sample, frame_samples, overlap_ratio)
    if sample <= frame_samples
        index = 1;
    else
        overlap_frame_samples = frame_samples * overlap_ratio;
        index = ceil(sample/overlap_frame_samples);
        ratio = frame_samples / overlap_frame_samples;
        index = index - ratio + 1; %Is plus one because in matlab the indexes starts at 1 and not 0
    end
end

function index = GetFirstBestIndexFromSample(sample, frame_samples, overlap_ratio)
    overlap_frame_samples = frame_samples * overlap_ratio;
    if sample <= overlap_frame_samples
        index = 1;
    else
        index = ceil(sample/overlap_frame_samples);
    end
end

function index = GetLastIndexFromSample(sample, frame_samples, overlap_ratio)
    if sample <= frame_samples
        index = 1;
    else
        overlap_frame_samples = frame_samples * overlap_ratio;
        index = ceil(sample/overlap_frame_samples);
    end
end

function index = GetLastBestIndexFromSample(sample, frame_samples, overlap_ratio)
    if sample <= frame_samples
        index = 1;
    else
        overlap_frame_samples = frame_samples * overlap_ratio;
        search_sample = sample - frame_samples;
        index = ceil(search_sample/overlap_frame_samples) + 1;
    end
end

function data = ConvertSecondsToSamples(data, FreqPerSec)
    data(:) = data(:) * FreqPerSec;
end