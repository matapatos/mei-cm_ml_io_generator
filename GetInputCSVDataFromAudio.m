% CONSTANT's DECLARATION
%FRAME_SIZE = 4096; %Number of samples for each frame
%CSV_DESTINATION = 'input.csv';


%filepath = 'Piano.wav';
%[mono, Fs] = ReadAudioAsMono(filepath);
%final_data = GetPreparedInputCSVData(mono, FRAME_SIZE);
%WriteCSVData(CSV_DESTINATION, final_data);

%FUNCTION's DECLARATION

function [final_data, Fs] = GetInputCSVDataFromAudio(filepath, frame_samples, overlap_ratio)
    [mono, Fs] = ReadAudioAsMono(filepath);
    final_data = GetPreparedInputCSVData(mono, frame_samples, overlap_ratio);
end

function [mono, Fs] = ReadAudioAsMono(filepath)
    [y, Fs] = audioread(filepath);
    mono = ConvertStereoToMono(y);
    mono = mono';
end

function data = GetPreparedInputCSVData(mono_audio, frame_size, overlap_ratio)

    current_nr_columns = size(mono_audio, 2);
    overlap_frame_size = frame_size * overlap_ratio;
    nr_of_final_rows = ceil(current_nr_columns/overlap_frame_size);

    data = zeros(nr_of_final_rows, frame_size);
    for i = 1 : nr_of_final_rows
        column_index = (i - 1) * overlap_frame_size + 1; %It's plus one because in MatLab the first index starts at 1
        end_column_index = column_index + frame_size - 1;
        if end_column_index > size(mono_audio, 2)
            end_column_index = size(mono_audio, 2);
        end
        max_columns_data = end_column_index - column_index + 1;

        data(i, 1:max_columns_data) = mono_audio(1, column_index:end_column_index);
    end
end

%Function only supports a number of maximum stereo channels as 3
%HOW IT WORKS: 
    % -> If sound already mono just returns it as it is.
    % -> If 2 or 3 channels exists it uses the mean of all the channels.
function mono = ConvertStereoToMono(stereo_sound)
    switch ndims(stereo_sound)
        case 1
            mono = stereo_sound;
        case 2
            mono = (stereo_sound(:,1) + stereo_sound(:,2)) / 2;
        case 3
            mono = (stereo_sound(:,1) + stereo_sound(:,2) + stereo_sound(:,3)) / 3;
        otherwise
            mono = [];
            error('Audio stereo dimension not supported. Number of dimensions: ' + ndims(stereo_sound))
    end
end






