classdef Metadata
    %METADATA Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        inputLocalMetadataCSVFile
        globalMetadataCSVFile
        globalMeta
        sepToken
        isToGenerateNotesDatasets
        isToGenerateSpecificFramesDatasets
        mainFolderSpecificNotesDataset
        globalMetasOfSpecificNotesDataset
        numberOfNotes
        firstNoteNumber
        datasetsNotesToBeCreated
        maxNrOfNotesPlayedSimultaneously
        configsFilepath
        frequency_samples
        frame_samples
        data_type
        overlap_ratio
    end
    
    methods
        function self = Metadata(globalMetadataCSVFile, inputLocalMetadataCSVFile, numberOfNotes, sepToken, isToGenerateNotesDatasets, isToGenerateSpecificFramesDatasets, mainFolderSpecificNotesDataset, firstNoteNumber, datasetsNotesToBeCreated, maxNrOfNotesPlayedSimultaneously, configsFilepath, frequency_samples, frame_samples, data_type, overlap_ratio)
            %METADATA Construct an instance of this class
            %   Detailed explanation goes here 
            self.maxNrOfNotesPlayedSimultaneously = maxNrOfNotesPlayedSimultaneously;
            self.globalMeta = GlobalMeta(numberOfNotes, self.maxNrOfNotesPlayedSimultaneously, firstNoteNumber, frequency_samples, frame_samples, data_type, overlap_ratio);
            self.globalMetadataCSVFile = globalMetadataCSVFile;
            self.inputLocalMetadataCSVFile = inputLocalMetadataCSVFile;
            self.numberOfNotes = numberOfNotes;
            self.sepToken = sepToken;
            self.isToGenerateNotesDatasets = isToGenerateNotesDatasets;
            self.isToGenerateSpecificFramesDatasets = isToGenerateSpecificFramesDatasets;
            self.mainFolderSpecificNotesDataset = mainFolderSpecificNotesDataset;
            self.globalMetasOfSpecificNotesDataset = self.GetArrayOfGlobalmetas();
            self.firstNoteNumber = firstNoteNumber;
            self.datasetsNotesToBeCreated = datasetsNotesToBeCreated;
            self.configsFilepath = configsFilepath;
            self.frequency_samples = frequency_samples;
            self.frame_samples = frame_samples;
            self.data_type = data_type;
            self.overlap_ratio = overlap_ratio;
            
            self.WriteAllLocalMetaFilesHeaders();
        end
        
        function [self, localMeta] = AppendLocalMetadataOfMusic(self, wavMusicFilename, inputData, outputData, nr_of_notes_per_music)
            localMeta = self.GetMetadataFromData(wavMusicFilename, inputData, outputData, nr_of_notes_per_music);
            %m_nrFrames, m_nrOfNotes, m_nrFramesWithNote,
            %m_nrFramesWithoutNote, m_nrNotesPerFrame, m_nrOfNotesById,
            %m_nrOfNotesPlayed, m_nrOfNotesPlayedByNoteId
            self.globalMeta = self.globalMeta.AddMusicMeta(localMeta.l_piano_type, localMeta.l_music_type, localMeta.g_nr_frames, localMeta.g_total_nr_notes, localMeta.g_nr_frames_with_note, localMeta.g_nr_frames_without_note, localMeta.g_nr_of_notes_per_frame, localMeta.g_nr_of_notes_by_type, localMeta.l_nr_of_notes_per_music, localMeta.g_number_of_notes_played, localMeta.g_number_of_notes_played_by_noteid);
            
            self.WriteNormalLocalMetaAsCSVFormat(localMeta);
                
            if self.isToGenerateNotesDatasets %Generate specific dataset
               self.globalMetasOfSpecificNotesDataset = self.AppendAndWriteMusicMetaBySpecificDataset(localMeta);
            end
        end
        
        function self = WriteGlobalMetadata(self) 
            self.globalMeta.WriteDataToFile(self.globalMetadataCSVFile, self.configsFilepath);
            if self.isToGenerateNotesDatasets %Write global metas for specific datasets
                noteIndex = self.firstNoteNumber - 1;
               for i = 1 : size(self.globalMetasOfSpecificNotesDataset, 2)
                   specificDatasetGlobalMeta = self.globalMetasOfSpecificNotesDataset(i).Value;
                   noteNumber = i + noteIndex;
                   if (isstring(self.datasetsNotesToBeCreated) && self.datasetsNotesToBeCreated == "all") || ismember(noteNumber, self.datasetsNotesToBeCreated)
                       self.CreateGlobalMetadataDataset(specificDatasetGlobalMeta, noteNumber);
                   end
               end
            end
        end
    end
    
    methods (Access = private)
        
        function CreateGlobalMetadataDataset(self, specificDatasetGlobalMeta, noteNumber)
            if isobject(specificDatasetGlobalMeta)
                baseFilename = strcat(self.mainFolderSpecificNotesDataset, "/" + num2str(noteNumber) + "_");
                metadataFilename = baseFilename + "global_metadata.csv";
                configsFilename = baseFilename + "configs.txt";
                specificDatasetGlobalMeta.WriteDataToFile(metadataFilename, configsFilename);
            end 
        end
        
        function globalMetas = GetArrayOfGlobalmetas(self)
           globalMetas = ObjectArray();
           if self.isToGenerateNotesDatasets
              for i = 1 : self.numberOfNotes
                globalMetas(i) = GlobalMeta(self.numberOfNotes, self.maxNrOfNotesPlayedSimultaneously, self.firstNoteNumber, self.frequency_samples, self.frame_samples, self.data_type, self.overlap_ratio);
              end
           end
        end
        
        function globalMetadatas = AppendAndWriteMusicMetaBySpecificDataset(self, localMeta)
            notes = localMeta.l_nr_of_notes_per_music;
            startNoteIndex = self.firstNoteNumber - 1;
            globalMetadatas = self.globalMetasOfSpecificNotesDataset;
            for i = 1 : size(notes, 2)
                n = notes(i);
                if n > 0
                    noteGlobalMetadata = globalMetadatas(i).Value;
                    noteNumber = startNoteIndex + i;
                    if (isstring(self.datasetsNotesToBeCreated) && self.datasetsNotesToBeCreated == "all") || ismember(noteNumber, self.datasetsNotesToBeCreated)
                        globalMetadatas(i) = self.CreateDatasetOfNote(noteGlobalMetadata, noteNumber, localMeta);
                    end
                end
            end
        end
        
        function noteGlobalMeta = CreateDatasetOfNote(self, noteGlobalMeta, noteNumber, localMeta)
            localMetadataFilepath = strcat(self.mainFolderSpecificNotesDataset, "/" + num2str(noteNumber) + "_train_local_metadata.csv");
            %Update GlobalMeta
            if noteGlobalMeta.nr_of_sounds == 0 %Is first time that it writes to this note dataset?!
                self.WriteLocalMetaHeaders(localMetadataFilepath);
            end
            %Update global meta
            noteGlobalMeta = noteGlobalMeta.AddMusicMeta(localMeta.l_piano_type, localMeta.l_music_type, localMeta.g_nr_frames, localMeta.g_total_nr_notes, localMeta.g_nr_frames_with_note, localMeta.g_nr_frames_without_note, localMeta.g_nr_of_notes_per_frame, localMeta.g_nr_of_notes_by_type, localMeta.l_nr_of_notes_per_music, localMeta.g_number_of_notes_played, localMeta.g_number_of_notes_played_by_noteid);
            %Write meta file
            self.WriteSpecificLocalMetaAsCSVFormat(localMeta, localMetadataFilepath);
        end
        % [Global] -> Metadata field that would be also used for global metadata of
        % the CSV file
        % [Local] -> Metadata field only used for music metadata itself
        function metadata = GetMetadataFromData(self, wavMusicFilename, inputData, outputData, nr_of_notes_per_music)
            metadata = [];
            metadata.g_nr_frames = size(inputData, 1); %[Global]
            metadata.g_nr_of_notes_by_type = sum(outputData(:,:)); %[Global]
            metadata.g_total_nr_notes = sum(metadata.g_nr_of_notes_by_type); %[Global]

            T_outputData = outputData';
            metadata.g_nr_of_notes_per_frame = sum(T_outputData(:,:)); %[Global]
            T_nr_notes_per_frame = metadata.g_nr_of_notes_per_frame';
            metadata.g_nr_frames_without_note = sum(T_nr_notes_per_frame(:,:) == 0); %[Global]
            metadata.g_nr_frames_with_note = metadata.g_nr_frames - metadata.g_nr_frames_without_note; %[Global]
            %nr_of_notes_played_by_music
       
            T_inputData = inputData';
            metadata.l_nr_of_non_zeros_per_frame = sum(T_inputData(:,:) ~= 0); %[Local] Maybe with this field we can discover when the music starts and stops
            metadata.l_music_type = MusicType.GetMusicTypeFromFilename(wavMusicFilename); %[Local] Used for determine if it's a nitid accord, noisy music or only noise.
            metadata.l_piano_type = MusicType.GetPianoTypeFromFilename(wavMusicFilename); %[Local]
            metadata.l_nr_of_notes_per_music = nr_of_notes_per_music;
            metadata.l_filename = self.GetFilename(wavMusicFilename);
            
            metadata.g_number_of_notes_played = zeros(self.maxNrOfNotesPlayedSimultaneously, 1);
            metadata.g_number_of_notes_played_by_noteid = zeros(self.maxNrOfNotesPlayedSimultaneously, self.numberOfNotes);
            for i = 1 : size(outputData, 1)
               frame = outputData(i, :);
               nr_of_notes_in_this_frame = sum(frame);
               if nr_of_notes_in_this_frame > 0                       
                   metadata.g_number_of_notes_played(nr_of_notes_in_this_frame) = metadata.g_number_of_notes_played(nr_of_notes_in_this_frame) + 1;
                   metadata.g_number_of_notes_played_by_noteid(nr_of_notes_in_this_frame, :) = metadata.g_number_of_notes_played_by_noteid(nr_of_notes_in_this_frame, :) + frame(1,:);
               end
            end
        end
        
        function filename = GetFilename(self, wavMusicFilename)
            [path, filename, ext] = fileparts(wavMusicFilename);
        end
        
        function WriteAllLocalMetaFilesHeaders(self)
            self.WriteLocalMetaHeaders(self.inputLocalMetadataCSVFile);
        end
        
        function WriteLocalMetaHeaders(self, filepath)
            headers = "filename,piano_type,music_type,nr_frames,nr_of_notes_by_frames,nr_frames_with_note,nr_frames_without_note,nr_notes_per_frame,nr_of_notes_by_noteid,nr_of_non_zeros_per_frame,nr_of_notes_per_music,nr_of_notes_played,nr_of_notes_played_by_noteid";
            fid = fopen(filepath, 'a');
            fprintf(fid, headers + "\n");
            fclose(fid);
        end
        
        function WriteLocalMetaAsCSVFormat(self, localData, filepath)
            fid = fopen(filepath, 'a');
            fprintf(fid, char(localData.l_filename));
            fprintf(fid, ",");
            fprintf(fid, char(localData.l_piano_type));
            fprintf(fid, ",");
            fprintf(fid, char(localData.l_music_type));
            fprintf(fid, ",");
            fprintf(fid, "%d", localData.g_nr_frames);
            fprintf(fid, ",");
            fprintf(fid, "%d", localData.g_total_nr_notes);
            fprintf(fid, ",");
            fprintf(fid, "%d", localData.g_nr_frames_with_note);
            fprintf(fid, ",");
            fprintf(fid, "%d", localData.g_nr_frames_without_note);
            fprintf(fid, ",");
            fprintf(fid, mat2str(localData.g_nr_of_notes_per_frame));
            fprintf(fid, ",");
            fprintf(fid, mat2str(localData.g_nr_of_notes_by_type));
            fprintf(fid, ",");
            fprintf(fid, mat2str(localData.l_nr_of_non_zeros_per_frame));
            fprintf(fid, ",");
            fprintf(fid, mat2str(localData.l_nr_of_notes_per_music));
            fprintf(fid, ",");
            fprintf(fid, mat2str(localData.g_number_of_notes_played));
            fprintf(fid, ",");
            fprintf(fid, mat2str(localData.g_number_of_notes_played_by_noteid) + "\n");
            fclose(fid);
        end
        
        function WriteNormalLocalMetaAsCSVFormat(self, localData)
            self.WriteLocalMetaAsCSVFormat(localData, self.inputLocalMetadataCSVFile);
        end
        
        function WriteSpecificLocalMetaAsCSVFormat(self, localData, filepath)
           self.WriteLocalMetaAsCSVFormat(localData, filepath);
        end
    end
end

