function all_files = GetAllWavAndTxtFilesFromFolder(folder, all_files)
    files_ = dir(folder);
    for i = 1 : size(files_, 1)
        f_ = files_(i:i);
        if strcmp(f_.name, '.') || strcmp(f_.name, '..') %Ignore current folder or previous ones from Linux.
            continue;
        end
       
        if f_.isdir
            folderPath = fullfile(f_.folder, f_.name);
            all_files = GetAllWavAndTxtFilesFromFolder(folderPath, all_files);
        elseif IsWav(f_.name)
            if HasTXTFileInFolder(f_.folder, f_.name)
                wavFilepath = fullfile(f_.folder, f_.name);
                txtFilename = GetTXTFilename(f_.name);               
                txtFilepath = fullfile(f_.folder, txtFilename);
                
                index = size(all_files,1) + 1;
                all_files(index, 1) = wavFilepath;
                all_files(index, 2) = txtFilepath;
            else        
                filepath = strcat(f_.folder, f_.name);
                warning(strcat('No TXT file found associated with: ', filepath));
            end
            
        end
    end
end

%Auxiliary methods

function isWAV = IsWav(filename)
    if endsWith(filename, '.wav')
        isWAV = true;
    else
        isWAV = false;
    end

end

function hasMidiFile = HasTXTFileInFolder(folder, wavFilename)

    midiFilename = GetTXTFilename(wavFilename);
    midiFullpath = fullfile(folder, midiFilename);
    status = exist(midiFullpath, 'file');
    
    if status == 2
        hasMidiFile = true;
    else 
        hasMidiFile = false;
    end

end

function txtFilename = GetTXTFilename(wavFilename)

    txtFilename = strrep(wavFilename, '.wav', '.txt');

end