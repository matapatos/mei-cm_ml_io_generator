classdef MusicType
    %MUSICTYPE Contains all the possible types of music available    
   enumeration
      SINGLE_NOTE, %ISOL
      CHORD_UCHO, %RAND
      CHORD_RAND, %UCHO
      MUSIC %MUS
   end
   
   methods(Static)
        function type = GetMusicTypeFromFilename(filename)
            if contains(filename, "_ISOL_")
                type = MusicType.SINGLE_NOTE;
                
            elseif contains(filename, "_MUS")
                type = MusicType.MUSIC;
                
            elseif contains(filename, "_RAND_")
                type = MusicType.CHORD_RAND;
            elseif  contains(filename, "_UCHO_")
                type = MusicType.CHORD_UCHO;
            else
                error = "ERROR: UNKOWN MUSIC TYPE " + filename
            end
            
        end  
        
        function type = GetPianoTypeFromFilename(filename)
            [filepath, name, ext] = fileparts(filename);
            if contains(name, "_")
                splitted = strsplit(name, '_');
                type = string(splitted(end));
            else
                error = "ERROR: The followinf filename don't use the right format " + filename
            end
        end
   end
   
end

